﻿using ECommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace ECommerce.Classes
{
    public class CombosHelper : IDisposable
    {
        private static ECommerceContext db = new ECommerceContext();
        public static List<Department> GetDepartments()
        {
            var departments = db.Departments.OrderBy(d => d.Name).ToList();
            departments.Add(new Department
            {
                DepartmentId = 0,
                Name = "[Select a department...]"
            });
            
            return departments.OrderBy(d => d.Name).ToList();
        }

        internal static List<Product> GetProducts(int companyId)
        {
            var product = db.Products.Where(p => p.CompanyId == companyId).ToList();
            product.Add(new Product
            {
                ProductId = 0,
                Description = "[Select a product...]"
            });

            return product.OrderBy(p => p.Description).ToList();
        }

        public static List<City> GetCities(int departmentId)
        {
            var cities = db.Cities.Where(c => c.DepartmentId == departmentId).OrderBy(d => d.Name).ToList();
            cities.Add(new City
            {
                CityId = 0,
                Name = "[Select a City...]"
            });

            return cities.OrderBy(d => d.Name).ToList();
        }

        public static List<Company> GetCompanies()
        {
            var companies = db.Companies.OrderBy(d => d.Name).ToList();
            companies.Add(new Company
            {
                CompanyId = 0,
                Name = "[Select a company...]"
            });

            return companies.OrderBy(d => d.Name).ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        internal static List<Customer> GetCustomers(int companyId)
        {
            var customers = db.Customers.Where(c => c.CompanyId == companyId).ToList();
            customers.Add(new Customer
            {
                CustomerId = 0,
                FirstName = "[Select a customer...]"
            });

            return customers.OrderBy(d => d.FirstName).ThenBy(c => c.LastName).ToList();
        }

        public static List<Category> GetCategories(int companyId)
        {
            var categories = db.Categories.Where(c => c.CompanyId == companyId).ToList();
            categories.Add(new Category
            {
                CategoryId = 0,
                Description = "[Select a category...]"
            });

            return categories.OrderBy(d => d.Description).ToList();
        }

        public static List<Tax> GetTaxes(int companyId)
        {
            var taxes = db.Taxes.Where(c => c.CompanyId == companyId).ToList();
            taxes.Add(new Tax
            {
                TaxId = 0,
                Description = "[Select a tax...]"
            });

            return taxes.OrderBy(d => d.Description).ToList();
        }
    }
}